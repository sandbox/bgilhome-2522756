(function ($) {

  // States
  $(document).bind('state:addclass', function(e) {
    // Need a dummy handler to trigger state-effect handler
  });
  $(document).bind('state:appendto', function(e) {
    // Need a dummy handler to trigger state-effect handler
  });

  // Effects
  $(document).bind('state:addclass-toggleclass', function(e) {
    if (e.trigger) {
      $(e.target).toggleClass(e.effect.class, e.value);
    }
  });
  $(document).bind('state:appendto-toggleappend', function(e) {
    // TODO this is not getting triggered currently
    if (e.trigger) {
      var $el = $(e.target);
      // Move element
      if (e.value) {
        // Save original DOM position
        $.data($el, 'parent', $el.parent());
        $.data($el, 'index', $el.index());
        // Now move
        var target = eval(e.effect.selector); // this is a jquery selector
        if (target.length) {
          $el.appendTo(target);
        }
      }
      // Move back to original DOM position
      else {
        var parent = $.data($el, 'parent');
        var index = $.data($el, 'index');
        if (typeof parent !== undefined) {
          if (typeof index !== undefined) {
            var target = parent.find(':nth-child(' + (index-1) + ')');
            if (target.length) {
              $(e.target).insertAfter(target);
            }
          }
          else {
            $(e.target).appendTo(parent);
          }
        }
        $(e.target).appendTo($(e.effect.selector))
      }
    }
  });

  // Conditions
  Drupal.states.Trigger.states.positiveInteger = {
    'keyup': function () {
      var val = this.val(),
          int = parseInt(val);
      return val == int && int > 0;
    }
  };

})(jQuery);
